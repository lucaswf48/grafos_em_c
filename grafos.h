#include <stdbool.h>

struct listagrafo{
	int info;
	struct lista *prox;
};

typedef struct listagrafo ListaGrafo;


typedef struct grafo{

	bool orientado;
	bool ponderado;
	int numero_vertices;
	int **matriz;

}Grafo;


typedef struct grafolista{

	//bool orientado;
	bool ponderado;
	int numero_vertices;
	ListaGrafo **vet_lista;

}GrafoLista;
