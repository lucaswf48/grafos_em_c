#include <stdio.h>
#include <stdlib.h>
#include "grafos.h"

Grafo* alocaGrafo(){

	return (Grafo*) malloc(sizeof(Grafo));
}

void alocaMatrizAdjacente(Grafo *g, int n,bool ori,bool pon){

	int cont;

	g->matriz = (int**)calloc(n,sizeof(int*));

	for(cont = 0;cont < n;++cont)
		g->matriz[cont] = (int*)calloc(n,sizeof(int));

	g->numero_vertices = n;
	g->orientado = ori;
	g->ponderado = pon;
}

void imprimeMatrizAdjacente(Grafo *g){

	int j,i;

	for(i = 0;i < g->numero_vertices;++i){
		printf("%d: ",i+1);
		for(j = 0;j < g->numero_vertices;++j){

			printf("%2d ",g->matriz[i][j]);
		}
		printf("\n");
	}
}

Grafo* insereMarizAdjacente(Grafo *g, int vertice1, int vertice2){


	int peso;

	if(g->orientado){

		if(g->ponderado){
			printf("Qual é preço do cabo %d para %d?",vertice1,vertice2);
			scanf("%d",&peso);
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = -1;
		}
		else{
			g->matriz[vertice1-1][vertice2-1] = 1;
			g->matriz[vertice2-1][vertice1-1] = -1;
		}
	}
	else{
		if(g->ponderado){
			printf("Qual é o peso de %d para %d:",vertice1,vertice2);
			scanf("%d",&peso);
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = peso;
			return g;
		}
		else{
			g->matriz[vertice1-1][vertice2-1] = 1;
			g->matriz[vertice2-1][vertice1-1] = 1;
		}
	}
}

Grafo* insereMarizAdjacente2(Grafo *g, int vertice1, int vertice2,int peso){



	if(g->orientado){

		if(g->ponderado){
			printf("Qual é preço do cabo %d para %d?",vertice1,vertice2);
			scanf("%d",&peso);
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = -1;
		}
		else{
			g->matriz[vertice1-1][vertice2-1] = 1;
			g->matriz[vertice2-1][vertice1-1] = -1;
		}
	}
	else{
		if(g->ponderado){
			/*printf("Qual é o peso de %d para %d:",vertice1,vertice2);
			scanf("%d",&peso);*/
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = peso;
			return g;
		}
		else{
			g->matriz[vertice1-1][vertice2-1] = 1;
			g->matriz[vertice2-1][vertice1-1] = 1;
		}
	}
}

bool avaliaVertices(Grafo *g,int a,int b){

	if((a >=0 && a <= g->numero_vertices) && (b >=0 && b <= g->numero_vertices))
		return true;
	else
		return false;
}

void imprimeCaracteristicas(Grafo *g){


	printf("Ponderado: %d\n",g->ponderado);
	printf("Orientado: %d\n",g->orientado);
	printf("Numero de vertices: %d\n",g->numero_vertices);

	printf("\n");
}

void grauVertice(Grafo *g){

	int j,i,conta_grau = 0;

	for(j = 0;j < g->numero_vertices;++j){
		for(i = 0;i < g->numero_vertices;++i){
			if(g->matriz[j][i] != 0 && g->matriz[j][i] != -1)
				++conta_grau;
		}
		printf("Grau do vertice %d: %d\n",j+1,conta_grau);
		conta_grau = 0;
	}
}

////////////////////////////////////////////////////////////////////////////

GrafoLista* alocaGrafoLista(){

	return (GrafoLista*)malloc(sizeof(GrafoLista));
}

void criaListaAdjacencias(GrafoLista *gl, int n,bool pon){

	int i;

	gl->vet_lista = (ListaGrafo**)malloc(n*sizeof(ListaGrafo));

	gl->numero_vertices = n;
	gl->ponderado = pon;

	for(i = 0; i < n;++i){
		gl->vet_lista[i] = listaCria();
	}
}

void insereListaAdjacencias(GrafoLista *gl,int a,int b){

	gl->vet_lista[a-1] = listaInsere(gl->vet_lista[a-1],b-1);
}

void imprimeListaAdjacencias(GrafoLista *gl){

	int a;

	for(a=0;a<gl->numero_vertices;++a){
		printf("Vertices adjacentes a %d\n",a+1);
		ListaGrafo *p;
		for(p = gl->vet_lista[a];p != NULL;p = p->prox){
			printf("Info %d \n", p->info + 1);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int buscaEmLargura(Grafo *g,int inicio){


	int visitado[g->numero_vertices];

	int ini,i;

	inicio--;

	int somatorio = 0;
	Grafo *teste = alocaGrafo();

	alocaMatrizAdjacente(teste,7,false,true);

	for(i = 0;i < g->numero_vertices;++i){
		visitado[i] = 0;
	}
	visitado[inicio] = -1;

	ListaGrafo *f = filaCria();
	filaInsere(f,inicio);

	while(!filaVazia(f)){
		i = filaRetira(f);
		for(ini = 0;ini < g->numero_vertices;++ini){

			if(g->matriz[i][ini] != 0 && visitado[ini]==0 && ini != i){

				somatorio += g->matriz[i][ini];

				printf("%d %d %d\n",i+1,ini+1,g->matriz[i][ini]);
				insereMarizAdjacente2(teste,i+1,ini+1,g->matriz[i][ini]);

				visitado[ini] = -1;
				filaInsere(f,ini);
			}
		}
		visitado[i] = 1;
	}
	imprimeMatrizAdjacente(teste);
	printf("Valor total? %d\n",somatorio);
}


Grafo* Kruskal(Grafo *g){

	int min,i,j,
		custo_minimo = 0,
		a=0,
		b=0,
		u=0,
		v=0,
		ne=1;

	int *parent = (int*)calloc(g->numero_vertices,sizeof(int));


	for(i=0;i<g->numero_vertices;i++)
	{
		for(j=0;j<g->numero_vertices;j++)
		{
			if(g->matriz[i][j]==0)
				g->matriz[i][j]=1000;
		}
	}

	while(ne < g->numero_vertices)
	{

		for(i=0,min=999;i<g->numero_vertices;i++)
		{
			for(j=0;j < g->numero_vertices;j++)
			{

				if(g->matriz[i][j] < min)
				{

					min=g->matriz[i][j];
					a=u=i;
					b=v=j;

				}
			}
		}
		u=find(u,parent);
		v=find(v,parent);
		if(uni(v,u,parent))
		{
			printf("Fio que liga %d a %d: R$ %d\n",a+1,b+1,min);
			custo_minimo +=min;
			ne++;
		}
		g->matriz[a][b]=g->matriz[b][a]=999;
	}
	free(g);
	g = NULL;
	printf("\nCusto minimo = %d\n",custo_minimo);

	return g;
}


int find(int i,int v[])
{
	while(v[i])
	i=v[i];
	return i;
}
int uni(int i,int j,int v[])
{
	if(i!=j)
	{
		v[j]=i;
		return 1;
	}
	return 0;
}
